
""" 
This is an implementation of a Tree Data Structure. 
In the tree data structure below we implement the following
sample tree structure

              5
           /    \
          3      2
        /  \   /  \
       9    1 7    6


Folling the tree, we implement some traversal techniques.
     - Pre-Order Traversal (Root -> Left -> Right)
     - In-Order Traversal (Left -> Root -> Right)


"""

class Node:

    def __init__(self, data=None):
        self.data = data 
        self.left = None
        self.right = None


class Tree:

    def __init__(self, root=None):
        self.root = Node(root)


    def visualize_tree(self, traversal_method):
        tree_values = ""
        if traversal_method == "preorder":
            return self.preorder_traversal(sample_tree.root, tree_values) 
        elif traversal_method == "inorder":
            return self.inorder_traversal(sample_tree.root, tree_values)
        elif traversal_method == "postorder":
            return self.postorder_traversal(sample_tree.root, tree_values)
        else:
            return f"{traversal_method} not implemented"


    def preorder_traversal(self, start, traversal):
        if start is not None:
            traversal += str(start.data) + ' -> '
            traversal = self.preorder_traversal(start.left, traversal) 
            traversal = self.preorder_traversal(start.right, traversal)
        return traversal


    def inorder_traversal(self, start, traversal):
        if start is not None:
            traversal = self.inorder_traversal(start.left, traversal)
            traversal += str(start.data)  + " -> "
            traversal = self.inorder_traversal(start.right, traversal)
        return traversal

    def postorder_traversal(self, start, traversal):
        if start is not None:
            traversal = self.inorder_traversal(start.left, traversal)
            traversal = self.inorder_traversal(start.right, traversal)
            traversal += str(start.data) + " -> "
        return traversal
    


# Implementing and sample Tree
sample_tree = Tree("5")
sample_tree.root.left = Node("3")
sample_tree.root.right = Node("2")

sample_tree.root.left.left = Node("9")
sample_tree.root.left.right = Node("1")

sample_tree.root.right.left = Node("7")
sample_tree.root.right.right = Node("6")


print(sample_tree.visualize_tree("preorder"))
print(sample_tree.visualize_tree("inorder"))
print(sample_tree.visualize_tree("postorder"))